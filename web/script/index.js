$(document).ready(function () {
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 50) {
            $('.nav').addClass('nav__scrolled');
        } else {
            $('.nav').removeClass('nav__scrolled');
        }

    });
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });
});

var portfolioItem = document.getElementsByClassName('portfolio__item');

for (i = 0; i < portfolioItem.length; i++) {
    var item = 'portfolioImg' + i;
    portfolioItem[i].setAttribute('id', 'triggerPoint-' + i);

}

var portfolioImg = document.getElementsByClassName('portfolio__img');

for (i = 0; i < portfolioImg.length; i++) {
    portfolioImg[i].classList.add('img-' + i);
}

var portfolioInfo = document.getElementsByClassName('portfolio__itemInfo');

for (i = 0; i < portfolioInfo.length; i++) {
    portfolioInfo[i].classList.add('itemInfo-' + i)

}

